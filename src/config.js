import Phaser from 'phaser';

export default { // Configura la pantalla del juego
    type: Phaser.AUTO,
    parent: 'game',
    backgroundColor: '#33A5E7',
    scale: {
        width: 800,
        height: 600,
        mode: Phaser.Scale.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH,
    },
    render: { // Modifica la vista del sprite 
        pixelArt: true,
    },
    physics: { // Configura fisicas
        default: 'arcade',
        arcade: { // Configura debug mode
            gravity: { y: 750 },
            debug: true,
            debugShowVelocity: true,
            debugShowBody: true,
            debugShowStaticBody: true,
        }
    }
};