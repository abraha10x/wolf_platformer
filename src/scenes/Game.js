import Phaser from 'phaser';

class Game extends Phaser.Scene {
    constructor() {
        super({ key: 'GameScene' }); // Construcor que da acceso a la clase 
    }

    preload() { // Metodo que carga sprites y su tamanio

        this.load.spritesheet('wolf-walk-sheet', 'assets/wolf/walk_anim.png', {
            frameWidth: 150,
            frameHeight: 150,
            //frameWidth: 107,
            //frameHeight: 118,
            //spacing: 43,
        });

    }

    create(data) { // Metodo que se utiliza para animar sprites

        this.anims.create({
            key: 'wolf_walking',
            frames: this.anims.generateFrameNumbers('wolf-walk-sheet'),
            frameRate: 5,
            repeat: -1,

        });

        this.player = this.physics.add.sprite(400, 300, 'wolf-walk-sheet');
        this.player.anims.play('wolf_walking');

        this.player.body.setCollideWorldBounds(true);
        this.player.body.setSize(35, 73); // Setea el tamanio de la box collition
        this.player.body.setOffset(45, 45); // setea la posiscion de la box collition

    }

    update(time, delta) {} // Metodo donde se crean inputs de controles u otras funciones 
}

export default Game;